﻿using Autodesk.DesignScript.Runtime;
using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.Interop;
using Autodesk.AECC.Interop.UiLand;
using System.Runtime.InteropServices;
using Autodesk.AECC.Interop.Land;
using Autodesk.DesignScript.Geometry;
using Autodesk.AECC.Interop.Roadway;
using Autodesk.AECC.Interop.UiRoadway;
using Autodesk.AEC.Interop.Base;
using Autodesk.AEC.Interop.UIBase;

namespace Civil3D
{
    public static class Assemblies
    {

        
        [MultiReturn(new[] { "Assemblies", "AssemblyNames", "AssemblyDescriptions" })]
        public static Dictionary<string, object> GetAssemblies(Object ActiveDocument)
        {
            AeccRoadwayDocument doc = ActiveDocument as AeccRoadwayDocument;
            AeccAssemblies assemblies = doc.Assemblies;

            List<AeccAssembly> assemblyList = new List<AeccAssembly>();
            List<string> assemblyNames = new List<string>();
            List<string> assemblyDescription = new List<string>();

            foreach (AeccAssembly a in assemblies)
            {
                assemblyList.Add(a);
                assemblyNames.Add(a.Name);
                assemblyDescription.Add(a.Description);
            }
            return new Dictionary<string, object>
            {
                { "Assemblies", assemblyList},
                { "AssemblyNames", assemblyNames},
                { "AssemblyDescriptions", assemblyDescription },
            };
        }

       
    }
}
