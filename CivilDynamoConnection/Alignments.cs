﻿using Autodesk.DesignScript.Runtime;
using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.Interop;
using Autodesk.AECC.Interop.UiLand;
using System.Runtime.InteropServices;
using Autodesk.AECC.Interop.Land;
using Autodesk.DesignScript.Geometry;
using Autodesk.AECC.Interop.Roadway;
using Autodesk.AECC.Interop.UiRoadway;
using Autodesk.AEC.Interop.Base;
using Autodesk.AEC.Interop.UIBase;

namespace Civil3D
{
    public static class Alignments
    {
        [MultiReturn(new[] { "Alignments", "AlignmentNames", "AlignmentDescriptions"})]
        public static Dictionary<string, object> GetAlignments(Object ActiveDocument)
        {
            List<AeccAlignment> alignments = new List<AeccAlignment>();
            List<string> alignmentNames = new List<string>();
            List<string> alignmentDescriptions = new List<string>();
            AeccDocument doc = ActiveDocument as AeccDocument;
            string msg = null;

            try
            {
                foreach (AeccAlignment al in doc.AlignmentsSiteless)
                {
                    alignments.Add(al);
                    alignmentNames.Add(al.Name);
                    alignmentDescriptions.Add(al.Description);
                }
            }
            catch (Exception ex)
            {
                msg = "Error : " + ex.Message;
            }

            return new Dictionary<string, object>
            {
                { "Alignments", alignments},
                { "AlignmentNames", alignmentNames},
                { "AlignmentDescriptions", alignmentDescriptions},
                { "Message",msg }
            };
        }

        [MultiReturn(new[] {"Stations", "StationEasting", "StationNorthing", "StationPoints", "AlignmentCurve", "StartingStation", "EndingStation", "AlignmentLength"})]
        public static Dictionary<string, object> GetAlignmentStations(Object Alignment, double StationType = 0, double MajorInterval = 20, double MinorInterval = 10)
        {
            AeccAlignment alignment = Alignment as AeccAlignment;
            AeccStationType stType = 0;
            switch (StationType)
            {
                case 0:
                    stType = AeccStationType.aeccMajor;
                    break;
                case 1:
                    stType = AeccStationType.aeccMinor;
                    break;
                case 2:
                    stType = AeccStationType.aeccAll;
                    break;
            }

            
            List<double> stationEasting = new List<double>();
            List<double> stationNorthing = new List<double>();
            List<Point> stationPoints = new List<Point>();
            double startingStation = alignment.StartingStation;
            double endingStation = alignment.EndingStation;
            double alignmentLength = alignment.Length;
            List<double> stationList = new List<double>();
            Curve alignmentCurve = null;
            string msg = null;

            try
            {
                AeccAlignmentStations stations = alignment.GetStations(stType, MajorInterval, MinorInterval);
                foreach (AeccAlignmentStation s in stations)
                {
                    stationList.Add(s.Station);
                    stationEasting.Add(s.Easting);
                    stationNorthing.Add(s.Northing);
                    stationPoints.Add(Point.ByCoordinates(s.Easting, s.Northing));
                }

                alignmentCurve = NurbsCurve.ByControlPoints(stationPoints);

            }
            catch (Exception ex)
            {
                msg = "Error : " + ex.Message;
            }

            return new Dictionary<string, object>
            {
                { "Stations", stationList},
                { "StationEasting", stationEasting},
                { "StationNorthing", stationNorthing},
                { "StationPoints", stationPoints },
                { "AlignmentCurve", alignmentCurve },
                { "StartingStation", startingStation },
                { "EndingStation", endingStation },
                { "AlignmentLength",alignmentLength }
            };
        }

        [MultiReturn(new[] { "Stations", "StationEasting", "StationNorthing", "StationPoints", "AlignmentCurve", "StartingStation", "EndingStation", "AlignmentLength" })]
        public static Dictionary<string, object> GetAlignmentStationsFromTo(Object Alignment, double FromStation, double ToStation, double StationType = 0, double MajorInterval = 20, double MinorInterval = 10)
        {
            AeccAlignment alignment = Alignment as AeccAlignment;
            AeccStationType stType = 0;
            switch (StationType)
            {
                case 0:
                    stType = AeccStationType.aeccMajor;
                    break;
                case 1:
                    stType = AeccStationType.aeccMinor;
                    break;
                case 2:
                    stType = AeccStationType.aeccAll;
                    break;

            }

            AeccAlignmentStations stations = alignment.GetStations(stType, MajorInterval, MinorInterval);
            List<double> stationEasting = new List<double>();
            List<double> stationNorthing = new List<double>();
            List<Point> stationPoints = new List<Point>();
            double startingStation = alignment.StartingStation;
            double endingStation = alignment.EndingStation;
            double alignmentLength = alignment.Length;
            List<double> stationList = new List<double>();
            Curve alignmentCurve = null;

            foreach (AeccAlignmentStation s in stations)
            {
                if (s.Station >= FromStation && s.Station <= ToStation)
                {
                    stationList.Add(s.Station);
                    stationEasting.Add(s.Easting);
                    stationNorthing.Add(s.Northing);
                    stationPoints.Add(Point.ByCoordinates(s.Easting, s.Northing));
                }
            }
            alignmentCurve = NurbsCurve.ByControlPoints(stationPoints);

            return new Dictionary<string, object>
            {
                { "Stations", stationList},
                { "StationEasting", stationEasting},
                { "StationNorthing", stationNorthing},
                { "StationPoints", stationPoints },
                { "AlignmentCurve", alignmentCurve },
                { "StartingStation", startingStation },
                { "EndingStation", endingStation },
                { "AlignmentLength",alignmentLength }
            };
        }

        [MultiReturn(new[] { "SampleLineGroups", "SampleLineGroupNames", "SampleLineGroupDescription", "SampleLineGroupLinePoints", "SampleLineGroupsCurves", "SampleLineGroupStations"})]
        public static Dictionary<string, object> GetSampleLineGroups(Object Alignment)
        {
            AeccAlignment alignment = Alignment as AeccAlignment;
            AeccSampleLineGroups sampleLineGroups = alignment.SampleLineGroups;
            List<AeccSampleLineGroup> sampleLineGroupsList = new List<AeccSampleLineGroup>();
            List<string> sampleLineGroupNames = new List<string>();
            List<string> sampleLineGroupsDescription = new List<string>();
            List<AeccSampleLines> sampleLineGroupLines = new List<AeccSampleLines>();
            List<List<Point>> points = new List<List<Point>>();
            List<double> sampleLineStations = new List<double>();
            List<Curve> sampleLineCurves = new List<Curve>();

            foreach (AeccSampleLineGroup s in sampleLineGroups)
            {
                sampleLineGroupsList.Add(s);
                sampleLineGroupNames.Add(s.Name);
                sampleLineGroupsDescription.Add(s.Description);
                foreach (AeccSampleLine l in s.SampleLines)
                {
                    List<Point> point = new List<Point>();
                    AeccSampleLineVertices vertices = l.Vertices;
                    foreach (AeccSampleLineVertex v in vertices)
                    {
                        point.Add(Point.ByCoordinates(v.Location[0],v.Location[1]));
                    }
                    sampleLineCurves.Add(Line.ByStartPointEndPoint(point[0], point[1]));
                    points.Add(point);
                    sampleLineStations.Add(l.Station);
                }
            }

            return new Dictionary<string, object>
            {
                { "SampleLineGroups", sampleLineGroupsList},
                { "SampleLineGroupNames", sampleLineGroupNames},
                { "SampleLineGroupDescription", sampleLineGroupsDescription},
                { "SampleLineGroupLinePoints", points},
                { "SampleLineGroupsCurves", sampleLineCurves},
                { "SampleLineGroupStations", sampleLineStations},
            };
        }

       
    }
}
