﻿using Autodesk.DesignScript.Runtime;
using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.Interop;
using Autodesk.AECC.Interop.UiLand;
using System.Runtime.InteropServices;
using Autodesk.AECC.Interop.Land;
using Autodesk.DesignScript.Geometry;
using Autodesk.AECC.Interop.Roadway;
using Autodesk.AECC.Interop.UiRoadway;
using Autodesk.AEC.Interop.Base;
using Autodesk.AEC.Interop.UIBase;
using Autodesk.Civil.ApplicationServices;

namespace Civil3D
{
    public static class Documents
    {

        [MultiReturn(new[] { "ActiveDocument","Documents","DocumentNames"})]
        public static Dictionary<string, object> GetDocuments(bool Refresh)
        {
            AcadDocument activeDoc = null;
            List<AeccDocument> documents = new List<AeccDocument>();
            List<string> documentNames = new List<string>();
            string msg = "";
            string error = "";


            if(Refresh)
            {
                try
                {
                    AcadApplication app = Marshal.GetActiveObject("AutoCAD.Application") as AcadApplication;
                    AeccRoadwayApplication m_aeccApp = app.GetInterfaceObject("AeccXUiRoadway.AeccRoadwayApplication.12.0") as AeccRoadwayApplication;

                    foreach (AeccDocument doc in m_aeccApp.Documents)
                    {
                        documentNames.Add(doc.Name);
                        documents.Add(doc);
                    }

                    activeDoc = m_aeccApp.ActiveDocument;
                }
                catch(Exception ex)
                {
                    error = ex.Message;
                }

                if (activeDoc == null)
                {
                    msg = "No active document available" + Environment.NewLine + "Error : " + error;
                }
                else
                {
                    msg = "Error : " + error;
                }

                }
            else
            {
                msg = "Set Refresh to True!";
            }

            return new Dictionary<string, object>
                {
                    { "ActiveDocument", activeDoc},
                    { "Documents", documents},
                    { "DocumentNames", documentNames},
                    { "Message",msg }
                };

        }
    }
}
