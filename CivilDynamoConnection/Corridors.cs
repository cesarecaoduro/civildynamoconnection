﻿using Autodesk.DesignScript.Runtime;
using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.Interop;
using Autodesk.AECC.Interop.UiLand;
using System.Runtime.InteropServices;
using Autodesk.AECC.Interop.Land;
using Autodesk.DesignScript.Geometry;
using Autodesk.AECC.Interop.Roadway;
using Autodesk.AECC.Interop.UiRoadway;
using Autodesk.AEC.Interop.Base;
using Autodesk.AEC.Interop.UIBase;

namespace Civil3D
{
    public class MessageFilter : IOleMessageFilter
    {
        //
        // Class containing the IOleMessageFilter
        // thread error-handling functions.

        // Start the filter.
        public static void Register()
        {
            IOleMessageFilter newFilter = new MessageFilter();
            IOleMessageFilter oldFilter = null;
            CoRegisterMessageFilter(newFilter, out oldFilter);
        }

        // Done with the filter, close it.
        public static void Revoke()
        {
            IOleMessageFilter oldFilter = null;
            CoRegisterMessageFilter(null, out oldFilter);
        }

        //
        // IOleMessageFilter functions.
        // Handle incoming thread requests.
        int IOleMessageFilter.HandleInComingCall(int dwCallType,
          System.IntPtr hTaskCaller, int dwTickCount, System.IntPtr
          lpInterfaceInfo)
        {
            //Return the flag SERVERCALL_ISHANDLED.
            return 0;
        }

        // Thread call was rejected, so try again.
        int IOleMessageFilter.RetryRejectedCall(System.IntPtr
          hTaskCallee, int dwTickCount, int dwRejectType)
        {
            if (dwRejectType == 2)
            // flag = SERVERCALL_RETRYLATER.
            {
                // Retry the thread call immediately if return >=0 & 
                // <100.
                return 99;
            }
            // Too busy; cancel call.
            return -1;
        }

        int IOleMessageFilter.MessagePending(System.IntPtr hTaskCallee,
          int dwTickCount, int dwPendingType)
        {
            //Return the flag PENDINGMSG_WAITDEFPROCESS.
            return 2;
        }

        // Implement the IOleMessageFilter interface.
        [DllImport("Ole32.dll")]
        private static extern int
          CoRegisterMessageFilter(IOleMessageFilter newFilter, out
          IOleMessageFilter oldFilter);
    }

    [ComImport(), Guid("00000016-0000-0000-C000-000000000046"),
    InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    interface IOleMessageFilter
    {
        [PreserveSig]
        int HandleInComingCall(
            int dwCallType,
            IntPtr hTaskCaller,
            int dwTickCount,
            IntPtr lpInterfaceInfo);

        [PreserveSig]
        int RetryRejectedCall(
            IntPtr hTaskCallee,
            int dwTickCount,
            int dwRejectType);

        [PreserveSig]
        int MessagePending(
            IntPtr hTaskCallee,
            int dwTickCount,
            int dwPendingType);
    }

    public static class Corridors
    {

        [MultiReturn(new[] { "Corridors", "CorridorNames", "CorridorDescriptions"})]
        public static Dictionary<string, object> GetCorridors(Object ActiveDocument)
        {
            AeccRoadwayDocument doc = ActiveDocument as AeccRoadwayDocument;
            AeccCorridors corridors = doc.Corridors;

            List<AeccCorridor> corridorList = new List<AeccCorridor>();
            List<string> corridorNames = new List<string>();
            List<string> corridorDescription = new List<string>();

            foreach (AeccCorridor c in corridors)
            {
                corridorList.Add(c);
                corridorNames.Add(c.Name);
                corridorDescription.Add(c.Description);
        

            }
            return new Dictionary<string, object>
            {
                { "Corridors", corridorList},
                { "CorridorNames", corridorNames},
                { "CorridorDescriptions", corridorDescription },
            };
        }

        [MultiReturn(new[] { "Baselines", "MainBaselineFeatureLines", "CodeNames", "Directions", "FeatureLinesPoints","FeatureLinesCurves" })]
        public static Dictionary<string, object> GetBaseline(Object Corridor, List<double> Stations, List<Point> StationPoints)
        {
            AeccCorridor corridor = Corridor as AeccCorridor;
            AeccBaselines baselines = corridor.Baselines;

            List<AeccBaseline>baselineList = new List<AeccBaseline>();
            List<AeccBaselineFeatureLines> baselineFeatureLines = new List<AeccBaselineFeatureLines>();
            List<string[]> codeNames = new List<string[]>();
            List<CoordinateSystem> directions = new List<CoordinateSystem>();
            List<List<Point>> fLinesPts = new List<List<Point>>();
            List<Curve> fLinesCurve = new List<Curve>();
            string msg = "Executed";

            foreach (AeccBaseline b in baselines)
            {
                baselineList.Add(b);
                
                baselineFeatureLines.Add(b.MainBaselineFeatureLines);
                codeNames.Add(b.MainBaselineFeatureLines.CodeNames);
                AeccFeatureLinesCol fLinesColl = b.MainBaselineFeatureLines.FeatureLinesCol as AeccFeatureLinesCol;
                try
                {
                    foreach (AeccFeatureLines fLine in fLinesColl)
                    {
                        foreach (AeccFeatureLine fL in fLine)
                        {
                            List<Point> pts = new List<Point>();
                            foreach (AeccFeatureLinePoint pt in fL.FeatureLinePoints)
                            {
                                if (pt.Station >= Stations[0] && pt.Station <= Stations[Stations.Count-1])
                                {
                                    pts.Add(Point.ByCoordinates((double)pt.XYZ[0], (double)pt.XYZ[1], (double)pt.XYZ[2]));
                                }
                            }
                            fLinesPts.Add(pts);
                            fLinesCurve.Add(NurbsCurve.ByControlPoints(pts));
                        } 
                    }
                }
                catch(Exception ex)
                {
                    msg = ex.Message;
                }
                
                for (int s = 0; s <= Stations.Count-1; s++)
                {
                    dynamic dir = b.GetDirectionAtStation(Stations[s]);
                    directions.Add(CoordinateSystem.ByOriginVectors(StationPoints[s], Vector.ByCoordinates(dir[0], dir[1], dir[2]), Vector.ZAxis()));
                }

            }

            return new Dictionary<string, object>
            {
                { "Baselines", baselineList},
                { "MainBaselineFeatureLines", baselineFeatureLines},
                { "CodeNames", codeNames},
                { "Directions", directions},
                { "FeatureLinesPoints", fLinesPts},
                { "FeatureLinesCurves", fLinesCurve},
                { "Exception", msg},
            };
        }

        [MultiReturn(new[] {"LinkCurves", "Stations", "Directions"})]
        public static Dictionary<string, object> GetAppliedAssembliesByLinks(Object Corridor)
        {
            AeccCorridor corridor = Corridor as AeccCorridor;

            AeccBaselines baselines = corridor.Baselines;

            //List<AeccBaseline> baselineList = new List<AeccBaseline>();
            //List<AeccBaselineFeatureLines> baselineFeatureLines = new List<AeccBaselineFeatureLines>(); 
            List<Vector> directions = new List<Vector>();
            List<List<Curve>> sectionLinks = new List<List<Curve>>();

            List<double> stations = new List<double>();
            List<double> shapeCount = new List<double>();
            string msg = "Executed";
            try
            {
                foreach (AeccBaseline b in baselines)
                {
                    AeccBaselineRegions bLRegions = b.BaselineRegions;
                    foreach (AeccBaselineRegion bLRegion in bLRegions)
                    {   
                        AeccAppliedAssemblies appAssemblies = bLRegion.AppliedAssemblies;
                        foreach (AeccAppliedAssembly appAss in appAssemblies)
                        {

                            AeccCalculatedLinks calcLinks = appAss.GetLinks();
                            List<Curve> links = new List<Curve>();

                            double station = 0;
                            foreach (AeccCalculatedLink cL in calcLinks)
                            {
                                List<Point> calcLinkPoints = new List<Point>();
                                AeccCalculatedPoints calcPoints = cL.CalculatedPoints;
                                foreach (AeccCalculatedPoint cP in calcPoints)
                                {
                                    calcLinkPoints.Add(Point.ByCoordinates(
                                        cP.GetStationOffsetElevationToBaseline()[0],
                                        cP.GetStationOffsetElevationToBaseline()[1],
                                        cP.GetStationOffsetElevationToBaseline()[2]
                                        ));
                                    station = cP.GetStationOffsetElevationToBaseline()[0];
                                }
                                links.Add(NurbsCurve.ByControlPoints(calcLinkPoints, 1));
                            }
                            sectionLinks.Add(links);
                            stations.Add(station);
                        }
                    }

                    foreach (double s in stations)
                    {
                        dynamic v = b.GetDirectionAtStation(s);
                        directions.Add(Vector.ByCoordinates(v[0], v[1], v[2]));
                    }
                }
               
            }
            catch(Exception ex)
            {
                msg = ex.Message;
            }

            return new Dictionary<string, object>
            {
                { "LinkCurves", sectionLinks},
                { "Stations", stations},
                { "Directions", directions},
                { "Exception", msg},
            };
        }

        [MultiReturn(new[] { "LinkCurves", "Stations"})]
        [IsVisibleInDynamoLibrary(false)]
        public static Dictionary<string, object> GetAppliedAssembliesByShapes(Object Corridor)
        {
            AeccCorridor corridor = Corridor as AeccCorridor;
            AeccBaselines baselines = corridor.Baselines;

            //List<AeccBaseline> baselineList = new List<AeccBaseline>();
            //List<AeccBaselineFeatureLines> baselineFeatureLines = new List<AeccBaselineFeatureLines>(); 
            List<CoordinateSystem> directions = new List<CoordinateSystem>();
            List<List<List<Curve>>> sectionLinks = new List<List<List<Curve>>>();

            List<double> stations = new List<double>();
            List<double> shapeCount = new List<double>();
            string msg = "Executed";
            try
            {
                foreach (AeccBaseline b in baselines)
                {
                    AeccBaselineRegions bLRegions = b.BaselineRegions;
                    foreach (AeccBaselineRegion bLRegion in bLRegions)
                    {
                        AeccAppliedAssemblies appAssemblies = bLRegion.AppliedAssemblies;
                        foreach (AeccAppliedAssembly appAss in appAssemblies)
                        {
                            List<List<Curve>> shapes = new List<List<Curve>>();
                            double station = 0;
                            foreach (AeccCalculatedShape cS in appAss.GetShapes())
                            {
                                List<Curve> links = new List<Curve>();
                                foreach (AeccCalculatedLink cL in cS.CalculatedLinks)
                                {
                                    List<Point> calcPoints = new List<Point>();
                                    foreach (AeccCalculatedPoint cP in cL.CalculatedPoints)
                                    {
                                        calcPoints.Add(
                                            Point.ByCoordinates(
                                                cP.GetStationOffsetElevationToBaseline()[0],
                                                cP.GetStationOffsetElevationToBaseline()[1],
                                                cP.GetStationOffsetElevationToBaseline()[2]
                                                )
                                                );

                                    }
                                    links.Add(NurbsCurve.ByControlPoints(calcPoints,1));
                                }
                                shapes.Add(links);
                            }
                            sectionLinks.Add(shapes);
                            stations.Add(station);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return new Dictionary<string, object>
            {
                { "LinkCurves", sectionLinks},
                { "Stations", stations},
                { "Exception", msg},
            };
        }
    }
}
