﻿//Created by Cesare Caoduro

using Autodesk.DesignScript.Runtime;
using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.Interop;
using Autodesk.AECC.Interop.UiLand;
using System.Runtime.InteropServices;
using Autodesk.AECC.Interop.Land;
using Autodesk.DesignScript.Geometry;
using Autodesk.AECC.Interop.Roadway;
using Autodesk.AECC.Interop.UiRoadway;
using Autodesk.AEC.Interop.Base;
using Autodesk.AEC.Interop.UIBase;

namespace Civil3D
{
    public static class Profiles
    {
        [MultiReturn(new[] { "Profiles", "ProfileNames", "ProfileDescriptions"})]
        public static Dictionary<string, object> GetAlignmentProfiles(Object Alignment)
        {
            AeccAlignment alignment = Alignment as AeccAlignment;
            AeccProfiles profiles = alignment.Profiles;
            List<AeccProfile> profileList = new List<AeccProfile>();
            List<string> profileNames = new List<string>();
            List<string> profileDescription = new List<string>();

            foreach (AeccProfile p in profiles)
            {
                profileList.Add(p);
                profileNames.Add(p.Name);
                profileDescription.Add(p.Description);
            }
            return new Dictionary<string, object>
            {
                { "Profiles", profileList},
                { "ProfileNames", profileNames},
                { "ProfileDescription", profileDescription },
            };


        }

        [MultiReturn(new[] { "Elevations", "ElevationMax", "ElevationMin", "ProfileLength"})]
        public static Dictionary<string, object> GetProfileElevationAtStations(Object Profile, List<double> Stations)
        {
            List<double> elevations = new List<double>();
            
            string msg = null;

            AeccProfile profile = Profile as AeccProfile;
            double maxEl = profile.ElevationMax;
            double minEl = profile.ElevationMin;
            double profileLength = profile.Length;


            foreach (double s in Stations)
            {
                try
                { elevations.Add(profile.ElevationAt(s)); }
                catch(Exception ex)
                { msg = ex.Message; }
            }
            
            return new Dictionary<string, object>
            {
                { "Elevations", elevations},
                { "ElevationMax", maxEl},
                { "ElevationMin", minEl},
                { "ProfileLength", profileLength},
            };

        }

        

       
    }
}
